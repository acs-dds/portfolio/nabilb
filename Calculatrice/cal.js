var a = document.getElementById("val1");
var b = document.getElementById("val2");
var res = document.getElementById("resultat");

document.getElementById("plus").addEventListener("click", addition);
document.getElementById("moins").addEventListener("click", soustraction);
document.getElementById("multiplier").addEventListener("click", multiplication);
document.getElementById("diviser").addEventListener("click", division);

function addition()	{
	 res.value = parseInt(a.value)+parseInt(b.value);
}
function soustraction()	{
	res.value = parseInt(a.value)-parseInt(b.value);
}
function multiplication()	{
	res.value = parseInt(a.value)*parseInt(b.value);
}
function division()	{
	res.value = parseInt(a.value)/parseInt(b.value);
}