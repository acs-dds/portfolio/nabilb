function vigenere(msg, key) {

	
	// transforme la lettre en valeur
	var charCode = msg.charCodeAt(0);
	/*var charKey = key.charCodeAt(0);*/

	// limitons le décalage à l'intervalle 0-25
	//var decalage = charKey % 26;

	// on vérifie si le caractère en cours est une lettre
	if (estUneLettre(charCode)) { // si c'est une lettre, on décale le caractère
		// charCode = 97 + (((charCode - 97) + (charKey - 97)) %26);
		charCode = (charCode + key) - 97;
			if (!estUneLettre(charCode)) { // si le caractère résultant n'est plus une lettre
				charCode -= 26; // on retire 26 et on retombe logiquement dans l'intervalle a-z (ou A-Z si c'était une majuscule)
				}
			}// si c'est pas une lettre, on ne le modifie pas

	return String.fromCharCode(charCode);
}


function estUneLettre(charCode) {
	return charCode > 64 && charCode < 91 // les maj
		|| charCode > 96 && charCode < 123; // les min

}


function encodeVig(msg, key) {
	
	var trans = "";

	for (i = 0; i < msg.length; i++)	{
			
			trans += vigenere(msg.charAt(i), key.charAt(i % key.length));
			
		}

		return trans;
}

function decodeVig(msg, key) {
	
	var trans = "";

	for (i = 0; i < msg.length; i++)	{
			
			trans += vigenere(msg.charAt(i), 26 - (key.charAt(i % key.length)));	
			
		}

		return trans;
}