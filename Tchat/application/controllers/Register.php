<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->has_userdata('user')) {
			redirect('http://nabilb.dijon.codeur.online/Tchat/Tchat');
		}
	}

	public function index() {

		$this->load->view('templates/header');
		$this->load->view('register');
		$this->load->view('templates/footer');
		
	}

	public function check() {
		$pwd = $this->input->post("pwd");
		$user = $this->input->post("user");

		$this->load->model('Register_Model');
		$cu = $this->Register_Model->checkUser($user,$pwd);
		$id = $this->Register_Model->checkId($user,$pwd);

		$data = array(
			        'user' => $cu,
			        'id_user' => $id
				);

		if(!$cu) {
			redirect(base_url());
		} else {
			$this->session->set_userdata($data);
			redirect('http://nabilb.dijon.codeur.online/Tchat/Tchat');
		}
	}

	public function lostPwd() {

		$user = $this->input->post('user');
		
		if (isset($user)) {
			
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    	$charactersLength = strlen($characters);
	    	$randomString = '';
	    	
	    	for ($i = 0; $i < 10; $i++) {
	        	$randomString .= $characters[rand(0, $charactersLength - 1)];
	    	}


			$this->load->model('Login_Model');
			$this->Login_Model->updatePwd($user, $randomString);

			echo 'Votre nouveau mot de passe est : '.$randomString. '<br /><a href="http://nabilb.dijon.codeur.online/Tchat">Retour</a>';

			/*$this->load->helper('email');

			$this->load->model('Register_Model');
			$mail = $this->Register_Model->checkEmail($user);
			$subject = 'Réinitialisation de votre mot de passe';
			$message = 'Votre nouveau mot de passe est :'.$randomString;
			
			send_mail($mail,$subject,$message);*/
			
		} else {
			redirect(base_url());
		}
	}

	public function newPwd() {
		$this->load->view('templates/header');
		$this->load->view('newpwd');
		$this->load->view('templates/footer');
	}
}

?>