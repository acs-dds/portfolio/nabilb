<?php

	class Control {

		private $user;

		public function __construct() {
			$CI =& get_instance();
			$this->user =& $CI->session;
		}
		
		public function check() {
			if ($this->user->has_userdata('user')) {
				return;
			}
			redirect(base_url());
		}

	}

?>