<?php

session_start();

class Message {

	private $nom;
	private $message;
	private $date;

	public function __construct ($nom, $message, $date) {
		$this->nom = $nom;
		$this->message = $message;
		$this->date = $date;
	}

	public function toArray () {
		return [$this->nom, $this->message, $this->date];
	}

	public function renderHTML() {	
		return "<h2>{$this->author} a dit</h2> <p>{$this->content}</p> le <time>".strftime("%A %e %B %H:%M:%S", $this->date)."</time>";
	}

	public function __toString() {
		return $this->renderHTML();
	}
}

///////////////////////////////////////////////////////

class MessageMapper {


	public function __construct ($nom, $message, $date) {
		
	}

	public function addMsg($message) {
		$file = fopen("/home/nabilb/AJAX/data/message.csv", "a");
		fputcsv($file, $message->toArray());
		fclose($file);
	}

	public function getMsg() {	
		$chat = [];
		$file = fopen("/home/nabilb/AJAX/data/message.csv", "r");

		while ($message = fgetcsv($file, 0)) {
			$chat[] = new Message($message[0], $message[1], $message[2]);
		}
		fclose($file);
		return $chat;
	}

}


////////////////////////////////////////////////////

class ChatController {
	private $mapper;

	public function __construct ($array) {
		session_start();
		setlocale(LC_TIME, 'fr_FR');
		$this->mapper = new MessageMapper();
	}

	public function register($nom) {
		$_SESSION["nom"] = $nom;
		return 0;
	}

	private function isRegistered() {
		return isset($_SESSION['nom']);
	}

	public function getHistory() {
		if (!$this->isRegistered()) return 2;
		$messages = $this->mapper->getMessages();
		$html = "";
		foreach ($messages as $message) {
			$html .= "<li>".$message->renderHtml()."</li>";
		}
		$_SESSION['last_update'] = microtime(true);
		return $html;
	}

	public function getNewMsg() {
		if (!$this->isRegistered()) return 2;
		// si un utilisateur appelle cette fonction avant getHistory(), $_SESSION['last_update'] ne sera pas défini => bug
		// du coup, pour ce cas-là, on "redirige" vers getHistory()
		if (!isset($_SESSION['last_update'])) return $this->getHistory();
		$messages = $this->mapper->getMessages($_SESSION['last_update']);
		$html = "";
		foreach ($messages as $message) {
			$html .= "<li>".$message->renderHtml()."</li>";
		}
		$_SESSION['last_update'] = microtime(true);
		return $html;
	}

	public function postMsg($message) {
		if (!$this->isRegistered()) return 2;
		$this->mapper->addMessage(
			$obj = new Message($_SESSION['nom'], microtime(true), $message)
		);
		return $this->getNewMessages();
	}
}

///////////////////////////////////////////////////


	if (count($_GET) > 0) {
		$c = new ChatController();
		if (isset($_GET['nom'])) {
			echo $c->register($_GET['nom']);
		}
		if (isset($_GET['message'])) {
			echo $c->postMessage($_GET['message']);
		}
		if (isset($_GET['get'])) {
			if (isset($_GET['new'])) {
				echo $c->getNewMessages();
			} else {
				echo $c->getHistory();
			}
		}
	}

?>