<div class="col-xs-12 achievo-list">
	<?php foreach($succes as $s): ?>
	<article id="progress">
		<h2><?= $s->titre.'<br />'; ?></h2>
		<p><?= $s->intitule.'<br />'; ?></p>
		<?php if ($s->progression >= $s->objectif){ ?>
			<p>Bravo objectif atteint !!</p>
			<img src="<?php echo site_url('img/trophee.png'); ?>" class="trophee">
		<?php } else {; ?>
		<div id="bar">
			<progress value="<?= $s->progression; ?>" max="<?= $s->objectif; ?>"></progress>
			<a href="<?php echo site_url('achievo/view/'.$s->id); ?>">View</a>
		</div>
		<?php }; ?>
	</article>
	<?php endforeach; ?>
</div>