<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Lorem Ipsum</title>
	<link style="stylesheet" href="style.css">
	<script type="text/javascript" src="jquery.min.js"></script>
	<script type="text/javascript" src="script.js"></script>
</head>
<body>
	<header>
		<fieldset>
			<h1>My Low Rem</h1>
		</fieldset>
	</header>
	<main>
		<form method="post" class="generate">
			<input type="number" class="mots" placeholder="Nombre de mots" max="3000" min="5" value="5" step="5">
			<input type="number" class="paragraphes" placeholder="Nombre de paragraphes" max="10" min="1" value="1">
			<select class="theme">
				<option value="lorem ipsum">Lorem Ipsum</option>
				<option value="ninja">Ninja</option>
			</select>
			<input type="submit">
		</form>
		<fieldset class="show">
		</fieldset>
	</main>
	<footer>
		<fieldset>
			<p>All Heil Britania</p>
		</fieldset>
	</footer>
</body>
</html>