<?php

class Lorem {

	private $letters /*"abcdefghijklmnopqrstuvwxyz"*/;
	/*private $last = ["am","um","or","us","at","i","it","et","a"];*/
	private $word;

	public function __construct($theme/* = "lorem ipsum"*/) {
		
		$this->letters = fgetcsv(fopen($theme, "r"), 0, ";");
	    $lettersLength = /*strlen*/count($this->letters);
	    /*$lastLength = count($this->last);*/
	    $randomWord = "";

	    /*for ($i = 0; $i < rand(0, 8); $i++) {*/
			$randomWord = $this->letters[rand(0, $lettersLength - 1)];
		/*}*/

		$this->word = $randomWord/* . $this->last[rand(0, $lastLength - 1)]*/;
	}

	public function getLorem() {
		return $this->word;
	}


}

class Mapper {

	private $lorem;

	public function __construct($theme/* = "lorem ipsum"*/) {
		$this->lorem = new Lorem($theme);
	}

	public function getPara($length, $para, $theme/* = "lorem ipsum"*/) {
		
		$paragraphes = [];

		for ($j=0; $j <= $para-1 ; $j++) { 
		
			$sentences = "";

			for ($i = 0; $i < $length; $i++) {
					$sentences .= $this->lorem->getLorem() . " ";
					$this->lorem = new Lorem($theme);
				}
			$sentences = ucfirst(trim($sentences)) . ".";

			$paragraphes[] .= $sentences;
		}

		ob_start();
		require ("paragraphe.phtml");
		return ob_get_clean();

	}
}

class Controller {

	private $msg;

	public function __construct ($theme) {
		$this->msg = new Mapper($theme);
	}

	public function newPara ($words, $para, $theme) {
		return $this->msg->getPara($words, $para, $theme);
	}

}

$words = $_POST["words"];
$para = $_POST["para"];
$theme = $_POST["theme"];

$gen = new Controller($theme);
echo $gen->newPara($words, $para, $theme);
?>