$(document).ready(function() {

	$(".generate").submit(function() {

		var mots = $(".mots").val();
		var paragraphes = $(".paragraphes").val();
		var choix = $(".theme").val();
		
		$.post("random.php",
			{
				words:mots,
				para:paragraphes,
				theme:choix
			},
				function(data) {
			$(".show").html(data);
		});

		return false;

		/*alert($(".mots").val() + " " + $(".paragraphes").val() +" " +  $(".theme").val());*/
	});
});