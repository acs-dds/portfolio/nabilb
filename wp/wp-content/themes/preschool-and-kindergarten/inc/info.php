<?php
/**
 * Preschool and Kindergarten Theme Info
 *
 * @package preschool_and_kindergarten
 */

function preschool_and_kindergarten_customizer_theme_info( $wp_customize ) {
	
    $wp_customize->add_section( 'theme_info' , array(
		'title'       => __( 'Information Links' , 'preschool-and-kindergarten' ),
		'priority'    => 6,
		));

	$wp_customize->add_setting('theme_info_theme',array(
		'default' => '',
		'sanitize_callback' => 'wp_kses_post',
		));
    
    $theme_info = '';
	$theme_info .= '<h3 class="sticky_title">' . __( 'Need help?', 'preschool-and-kindergarten' ) . '</h3>';
    $theme_info .= '<span class="sticky_info_row"><label class="row-element">' . __( 'View demo', 'preschool-and-kindergarten' ) . ': </label><a href="' . esc_url( 'http://raratheme.com/previews/?theme=preschool-and-kindergarten' ) . '" target="_blank">' . __( 'here', 'preschool-and-kindergarten' ) . '</a></span><br />';
	$theme_info .= '<span class="sticky_info_row"><label class="row-element">' . __( 'View documentation', 'preschool-and-kindergarten' ) . ': </label><a href="' . esc_url( 'http://raratheme.com/documentation/preschool-and-kindergarten/' ) . '" target="_blank">' . __( 'here', 'preschool-and-kindergarten' ) . '</a></span><br />';
    $theme_info .= '<span class="sticky_info_row"><label class="row-element">' . __( 'Theme info', 'preschool-and-kindergarten' ) . ': </label><a href="' . esc_url( 'https://raratheme.com/wordpress-themes/preschool-and-kindergarten/' ) . '" target="_blank">' . __( 'here', 'preschool-and-kindergarten' ) . '</a></span><br />';
    $theme_info .= '<span class="sticky_info_row"><label class="row-element">' . __( 'Support ticket', 'preschool-and-kindergarten' ) . ': </label><a href="' . esc_url( 'https://raratheme.com/support-ticket/' ) . '" target="_blank">' . __( 'here', 'preschool-and-kindergarten' ) . '</a></span><br />';
	$theme_info .= '<span class="sticky_info_row"><label class="row-element">' . __( 'Rate this theme', 'preschool-and-kindergarten' ) . ': </label><a href="' . esc_url( 'https://wordpress.org/support/theme/preschool-and-kindergarten/reviews' ) . '" target="_blank">' . __( 'here', 'preschool-and-kindergarten' ) . '</a></span><br />';
	$theme_info .= '<span class="sticky_info_row"><label class="more-detail row-element">' . __( 'More WordPress Themes', 'preschool-and-kindergarten' ) . ': </label><a href="' . esc_url( 'https://raratheme.com/wordpress-themes/' ) . '" target="_blank">' . __( 'here', 'preschool-and-kindergarten' ) . '</a></span><br />';


	$wp_customize->add_control( new preschool_and_kindergarten_Theme_Info( $wp_customize ,'theme_info_theme',array(
		'label' => __( 'About Preschool and Kindergarten' , 'preschool-and-kindergarten' ),
		'section' => 'theme_info',
		'description' => $theme_info
		)));

	$wp_customize->add_setting('theme_info_more_theme',array(
		'default' => '',
		'sanitize_callback' => 'wp_kses_post',
		));

}
add_action( 'customize_register', 'preschool_and_kindergarten_customizer_theme_info' );


if( class_exists( 'WP_Customize_control' ) ){

	class preschool_and_kindergarten_Theme_Info extends WP_Customize_Control
	{
    	/**
       	* Render the content on the theme customizer page
       	*/
       	public function render_content()
       	{
       		?>
       		<label>
       			<strong class="customize-text_editor"><?php echo esc_html( $this->label ); ?></strong>
       			<br />
       			<span class="customize-text_editor_desc">
       				<?php echo wp_kses_post( $this->description ); ?>
       			</span>
       		</label>
       		<?php
       	}
    }//editor close
    
}//class close

